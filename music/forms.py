from django import forms
from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:   ## contains info about the class
        model = User  ## Using existing User model
        fields = ['username', 'email', 'password']
