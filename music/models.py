from django.db import models
from django.core.urlresolvers import reverse
# AFTER EVERY CHANGE APPLIED:
# python manage.py makemigrations music
# python manage.py sqlmigrate music 0001  - SQL look (if needed)
# python manage.py migrate  -  run migrations


class Album(models.Model):
    artist = models.CharField(max_length=250)       # table columns
    album_title = models.CharField(max_length=500)
    genre = models.CharField(max_length=100)
    album_Logo = models.FileField()          # CharField(max_length=1000)

    def get_absolute_url(self):
        return reverse('music:detail', kwargs={'pk': self.pk})

    def __str__(self):                                  # prints specified column NAMES in shell
        return self.album_title + ' - ' + self.artist


class Song(models.Model):                                       # song is a part of an album
    album = models.ForeignKey(Album, on_delete=models.CASCADE)  # delete all songs from a deleted album
    file_type = models.CharField(max_length=10)
    song_title = models.CharField(max_length=250)
    is_favorite = models.BooleanField(default=False)

    def __str__(self):
        return self.song_title