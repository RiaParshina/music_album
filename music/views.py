# from django.http import Http404
#from django.template import loader
from django.views.generic.edit import CreateView, UpdateView, DeleteView  # Modal Form
from django.core.urlresolvers import reverse_lazy
from .models import Album, Song
from .forms import UserForm
from django.views import generic
from django.views.generic import View
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login


class IndexView(generic.ListView):
    template_name = 'music/index.html'
    context_object_name = 'all_albums'  # replaces default object_list name

    def get_queryset(self):
        return Album.objects.all()  # returns object_list by default


class DetailView(generic.DetailView):
    model = Album
    template_name = 'music/detail.html'


class AlbumCreate(CreateView):
    model = Album;  # What is created?
    fields = ['artist', 'album_title', 'genre', 'album_Logo']  # which there fields will be to fill in?


class AlbumUpdate(UpdateView):
    model = Album;
    fields = ['artist', 'album_title', 'genre', 'album_Logo']


class AlbumDelete(DeleteView):
    model = Album;
    success_url = reverse_lazy('music:index')


class UserFormView(View):
    form_class = UserForm
    template_name = 'music/registration_form.html'

    # display blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    # process form data
    def post(self, request):
        form = self.form_class(request.POST)  # built in Django function

        if form.is_valid():
            user = form.save(commit=False)  # Creates an obj from form without saving it to DB
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user.set_password(password)
            user.save()

            # returns a user if the data is correct

            user = authenticate(username=username, password=password)  # takes username & psw and check DB if they exist
            if user is not None:
                if user.is_active:
                    login(request, user)  # do login
                    return redirect('music:index')

        return render(request, self.template_name, {'form':form})













